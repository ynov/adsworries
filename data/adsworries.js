// jQuery 1.9.1 ready

jQuery.noConflict();
var doAdRemoval = function($, Data) {

    var removeAd = function(node, attrName, dataPatterns) {
        if (dataPatterns === undefined) {
            dataPatterns = Data.urlPatterns;
        }

        var attrVal = $(node).attr(attrName);

        if (attrVal) {
            attrVal = attrVal.toLowerCase();

            for (var i = 0; i < dataPatterns.length; i++) {
                var pattern = dataPatterns[i];
                var re = new RegExp(pattern);

                if (re.exec(attrVal) != null) {
                    $(node).remove();
                }
            }
        }
    };

    $('iframe').each(function(index) {
        removeAd(this, 'src');
        removeAd(this, 'name', Data.wordPatterns);
        removeAd(this, 'id', Data.wordPatterns);
    });

    $('embed').each(function(index) {
        removeAd(this, 'src');
        removeAd(this, 'id', Data.wordPatterns);
        removeAd(this, 'flashvars', Data.wordPatterns);
    });

    $('a').each(function(index) {
        removeAd(this, 'href');
    });

    $('div').each(function(index) {
        removeAd(this, 'id', Data.wordPatterns);
        removeAd(this, 'class', Data.wordPatterns);
    });

};

self.port.once('dataSeed', function(data) {
    window.Data = data;
    self.port.emit('dataReady');
});

self.port.once('doAdRemoval', function() {
    doAdRemoval(jQuery, window.Data);
});
