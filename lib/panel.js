var self = require('sdk/self');
var data = require('./data').Data;
var storage = require('./storage').Storage;

var panel = require('sdk/panel').Panel({
    width: 480,
    height: 480,
    contentURL: self.data.url('panel.html'),
    contentScriptFile: [
        self.data.url('jquery-1.9.1.min.js'),
        self.data.url('panel-client.js')
    ]
});

panel.port.on('dataRequest', function() {
    panel.port.emit('dataResponse', storage.Data);
});

panel.port.on('resetData', function() {
    storage.Data = data;
    panel.port.emit('resetDataResponse', storage.Data);
});

panel.port.on('saveData', function(newData) {
    storage.Data = newData;
    panel.port.emit('saveDataResponse', storage.Data);
});

exports.Panel = panel;
