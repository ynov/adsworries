var self = require('sdk/self');
var panel = require('./panel').Panel;

var widget = require('sdk/widget').Widget({
    id: 'adsworries',
    label: 'AdsWorries Options',
    contentURL: self.data.url('img/adsworries16.png'),
    onClick: function() {
        panel.show();
    }
});

exports.Widget = widget;
