var loadPatterns = function(eId, patterns) {
    var patternsText = '';

    for (var i = 0; i < patterns.length; i++) {
        patternsText += patterns[i];
        if (i != patterns.length - 1) {
            patternsText += '\n';
        }
    }

    $(eId).val(patternsText);
};

var showNotification = function(message) {
    var box = $('#notification-box');
    box.html(message).fadeIn();
    setTimeout(function() {
        box.fadeOut();
    }, 1000);
};

var reloadViews = function(data) {
    loadPatterns('#url-patterns', data.urlPatterns);
    loadPatterns('#word-patterns', data.wordPatterns);
    $('#data-box').html(JSON.stringify(data, null, 4));
};

$(document).ready(function() {
    self.port.emit('dataRequest');
    self.port.on('dataResponse', function(data) {
        reloadViews(data);
    });
});

$('#reset-button').click(function() {
    self.port.emit('resetData');
    self.port.on('resetDataResponse', function(data) {
        reloadViews(data);
        showNotification('Reset success!');
    });
});

$('#save-button').click(function() {
    var _urlPatterns = $('#url-patterns').val().split('\n');
    var _wordPatterns = $('#word-patterns').val().split('\n');

    var newData = { urlPatterns: _urlPatterns, wordPatterns: _wordPatterns };

    self.port.emit('saveData', newData);
    self.port.on('saveDataResponse', function(data) {
        reloadViews(data);
        showNotification('Patterns have been saved!');
    });
});

$('#data-box-toggle').click(function() {
    $('#data-box').toggle();
});
