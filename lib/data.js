var Data = {
    urlPatterns: [
        'ad\\.doubleclick\\.net',
        '.+\\.g\\.doubleclick\\.net',
        'googleadservices\\.com',
        '.+\\.tribalfusion\\.com',
        'adnxs\\.com',
        'adinterax\\.com',
        'connexity\\.net',
        'adserver\\.adreactor\\.com',
        'adreactor/servlet',
        'ad\\.propellerads\\.com',
        'adcash\\.com/script',
        'ads\\.cnn\\.com',
        'adtech\\..+/adlink',
        'affiliates\\.jlist\\.com',
        'ads\\.contentabc\\.com',
        'go\\.game321\\.com',
        'ad\\.mangareader\\.net',
        'espressogossip\\.com',
        'mgid\\.com'
    ],

    wordPatterns: [
        '[_ ]ad',
        'ad[sx_ ]',
        'ad(vert|vertisement|reactor|chapter)',
        '(videocube|marketgid)',
    ]
};

exports.Data = Data;
