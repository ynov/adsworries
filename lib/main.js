var self = require('sdk/self');
var pageMod = require('sdk/page-mod');

var widget = require('./widget').Widget;

var data = require('./data').Data;
var storage = require('./storage').Storage;

if (!storage.Data) {
    storage.Data = data;
}

pageMod.PageMod({
    include: ['*'],
    onAttach: function(worker) {
        worker.port.emit('dataSeed', storage.Data);
        worker.port.once('dataReady', function() {
            worker.port.emit('doAdRemoval');
        });
    },
    contentScriptFile: [
        self.data.url('jquery-1.9.1.min.js'),
        self.data.url('adsworries.js')
    ]
});
